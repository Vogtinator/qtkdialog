#include <stdio.h>
#include <dlfcn.h>

#include <QApplication>
#include <QFileDialog>

#define GTK_OVERRIDE extern "C" Q_DECL_EXPORT

typedef size_t GType;

struct GtkDialog {
    GType *type;
};

typedef void GtkFileChooser;
typedef enum
{
  GTK_FILE_CHOOSER_ACTION_OPEN,
  GTK_FILE_CHOOSER_ACTION_SAVE,
  GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,
  GTK_FILE_CHOOSER_ACTION_CREATE_FOLDER
} GtkFileChooserAction;

static void (*real_ffi_call)(void *cif, void (*fn)(void), void *rvalue, void **avalue);
static GType (*real_g_type_from_name)(const char *);
static int (*real_gtk_dialog_run)(GtkDialog *dialog);
static char *(*real_gtk_file_chooser_get_filename)(GtkFileChooser *chooser);
static GtkFileChooserAction (*real_gtk_file_chooser_get_action)(GtkFileChooser *chooser);
static void (*real_gtk_main_quit)();
static bool (*real_g_type_is_a)(GType a, GType b);

static bool init()
{
    real_ffi_call = (decltype(real_ffi_call))dlsym(RTLD_NEXT, "ffi_call");
    real_g_type_from_name = (decltype(real_g_type_from_name))dlsym(RTLD_NEXT, "g_type_from_name");
    real_gtk_dialog_run = (decltype(real_gtk_dialog_run))dlsym(RTLD_NEXT, "gtk_dialog_run");
    real_gtk_file_chooser_get_filename = (decltype(real_gtk_file_chooser_get_filename))dlsym(RTLD_NEXT, "gtk_file_chooser_get_filename");
    real_gtk_file_chooser_get_action = (decltype(real_gtk_file_chooser_get_action))dlsym(RTLD_NEXT, "gtk_file_chooser_get_action");
    real_g_type_is_a = (decltype(real_g_type_is_a))dlsym(RTLD_NEXT, "g_type_is_a");
    real_gtk_main_quit = (decltype(real_gtk_main_quit))dlsym(RTLD_NEXT, "gtk_main_quit");
    return real_ffi_call && real_g_type_from_name && real_gtk_dialog_run
            && real_gtk_file_chooser_get_filename && real_gtk_file_chooser_get_action
            && real_g_type_is_a && real_gtk_main_quit;
}

// Null -> No Qt dialog used
// Empty -> No file chosen
// Non-empty -> Chosen filename
static QString result;
static QApplication *app = nullptr;

GTK_OVERRIDE
int gtk_dialog_run(GtkDialog *dialog)
{
    result = QString();

    static bool inited = init();
    if(!inited)
        inited = init();

    if(!inited)
        return real_gtk_dialog_run(dialog);

    if(*dialog->type != real_g_type_from_name("GtkFileChooserDialog"))
        return real_gtk_dialog_run(dialog);

    GtkFileChooserAction action = real_gtk_file_chooser_get_action(dialog);

    if(action != GTK_FILE_CHOOSER_ACTION_OPEN
            && action != GTK_FILE_CHOOSER_ACTION_SAVE)
        return real_gtk_dialog_run(dialog);

    static int argc = 1;
    static char* argv[2] = {strdup(""), nullptr};
    if(!app)
        app = new QApplication(argc, argv);

    if(action == GTK_FILE_CHOOSER_ACTION_SAVE)
        result = QFileDialog::getSaveFileName();
    else if(action == GTK_FILE_CHOOSER_ACTION_OPEN)
        result = QFileDialog::getOpenFileName();

    if(result.isNull())
    {
        result = QStringLiteral("");
        return -2; // REJECT
    }

    return -3; // ACCEPT
}

GTK_OVERRIDE
void gtk_main_quit()
{
    if(app)
        delete app;

    return real_gtk_main_quit();
}

GTK_OVERRIDE
char *gtk_file_chooser_get_filename(GtkDialog *dialog)
{
    static bool inited = init();
    if(!inited)
        inited = init();

    if(!inited || result.isNull())
        return real_gtk_file_chooser_get_filename(dialog);

    if(result.isEmpty())
        return nullptr;

    return strdup(result.toUtf8().data());
}

GTK_OVERRIDE
void ffi_call(void *cif, void (*fn)(void), void *rvalue, void **avalue)
{
    static bool inited = init();
    if(!inited)
        inited = init();

    if((void*)fn == (void*)real_gtk_dialog_run)
        return real_ffi_call(cif, (void (*)())gtk_dialog_run, rvalue, avalue);

    if((void*)fn == (void*)real_gtk_file_chooser_get_filename)
        return real_ffi_call(cif, (void (*)())gtk_file_chooser_get_filename, rvalue, avalue);

    return real_ffi_call(cif, fn, rvalue, avalue);
}

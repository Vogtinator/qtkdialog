#ifndef QTKDIALOG_GLOBAL_H
#define QTKDIALOG_GLOBAL_H

#include <QtCore/qglobal.h>

#define GTK_OVERRIDE extern "C" Q_DECL_EXPORT

#endif // QTKDIALOG_GLOBAL_H
